package gocrm

type Event struct {
	Note      string `json:"Note"`
	EventType int    `json:"EventType"`
}

type Events []Event
