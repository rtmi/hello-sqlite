package gocrm

import (
	"database/sql"
	"fmt"
	_ "github.com/mattn/go-sqlite3"
	"log"
	"os"
	"strconv"
)

const (
	DB_DRIVER    = "sqlite3"
	DB_DSN       = ":memory:"
	DB_QCOMPANY  = "select id, name, status from company"
	DB_QCOMPANYK = "select * from company where id = ?"
	DB_QCONTACT  = "select id, firstname, lastname from contact"
	DB_ICONTACT  = "insert into contact(firstname, lastname) values('Carl', 'Sagan'), ('Neil', 'DeGrass-Tyson'), ('Ray', 'Parsec')"
	DB_ICOMPANY  = "insert into company(name, status) values('Acme Inc', 11)"
	DB_MCOMPANY  = "insert into company(name, status) values(?, ?)"
	DB_UCOMPANY  = "update company set name=?, status=? WHERE id=?"
	DB_DCOMPANY  = "create table if not exists company(id integer primary key, name text NOT NULL, status integer DEFAULT 0)"
	DB_DCONTACT  = "create table if not exists contact(id integer primary key, firstname text, lastname text, email text UNIQUE, work text, cell text)"
	DB_DEVENT    = "create table if not exists event(id integer primary key, note text, eventype integer, companyid integer, date text DEFAULT CURRENT_TIMESTAMP)"
	DB_DCO2CT    = "create table if not exists companycontact(companyid integer, contactid integer)"
	DB_DEV2CT    = "create table if not exists eventcontact(eventid integer, contactid integer)"
)

var (
	crmdb *sql.DB
	dbdsn string
)

func Init() {
	configEnv()
	db, err := sql.Open(DB_DRIVER, dbdsn)
	if err != nil {
		log.Fatal(err)
	}
	crmdb = db
	exec(DB_DCOMPANY)
	exec(DB_DCONTACT)
	exec(DB_DEVENT)
	exec(DB_ICOMPANY)

}

func Quit() {
	crmdb.Close()
}

func configEnv() {
	dbdsn = os.Getenv("DBDSN")
	if dbdsn == "" {
		dbdsn = DB_DSN
	}
}

func FetchCompanyByKey(key string) Companies {

	stmt, err := crmdb.Prepare(DB_QCOMPANYK)
	if err != nil {
		log.Fatal(err)
	}
	defer stmt.Close()

	var (
		id     int
		name   string
		status int
	)
	err = stmt.QueryRow(key).Scan(&id, &name, &status)
	if err != nil {
		log.Fatal(err)
	}

	a := Attr{name, status}
	c := Company{"companies", strconv.Itoa(id), a}
	ls := []Company{c}
	return Companies{ls}
}

func FetchCompany() Companies {
	rows, err := crmdb.Query(DB_QCOMPANY)
	if err != nil {
		log.Fatal(err)
	}
	defer rows.Close()
	ls := []Company{}

	for rows.Next() {
		var (
			id     int
			name   string
			status int
		)
		err = rows.Scan(&id, &name, &status)
		if err != nil {
			log.Fatal(err)
		}

		a := Attr{name, status}
		c := Company{"companies", strconv.Itoa(id), a}
		ls = append(ls, c)
	}
	err = rows.Err()
	if err != nil {
		log.Fatal(err)
	}
	return Companies{ls}
}

func FetchContact() Contacts {
	rows, err := crmdb.Query(DB_QCONTACT)
	if err != nil {
		log.Fatal(err)
	}
	defer rows.Close()
	ls := Contacts{}
	for rows.Next() {
		var (
			id    int
			fname string
			lname string
		)
		err = rows.Scan(&id, &fname, &lname)
		if err != nil {
			log.Fatal(err)
		}
		contact := Contact{Firstname: fname, Lastname: lname}
		ls = append(ls, contact)
	}
	err = rows.Err()
	if err != nil {
		log.Fatal(err)
	}
	return ls
}

func AddCompany(co Company) {
	stmt, err := crmdb.Prepare(DB_MCOMPANY)
	if err != nil {
		log.Fatal(err)
	}
	defer stmt.Close()

	_, err = stmt.Exec(co.Attributes.Name, co.Attributes.Status)
	if err != nil {
		log.Fatal(err)
	}
}

func UpdateCompany(co Company) {
	stmt, err := crmdb.Prepare(DB_UCOMPANY)
	if err != nil {
		log.Fatal(err)
	}
	defer stmt.Close()

	_, err = stmt.Exec(co.Attributes.Name, co.Attributes.Status, co.Id)
	if err != nil {
		log.Fatal(err)
	}
}

func exec(sql string) {
	_, err := crmdb.Exec(sql)
	if err != nil {
		log.Fatal(err)
	}
}

////////
////////

func exampleTransaction(db *sql.DB) {
	tx, err := db.Begin()
	if err != nil {
		log.Fatal(err)
	}
	stmt, err := tx.Prepare(DB_MCOMPANY)
	if err != nil {
		log.Fatal(err)
	}
	defer stmt.Close()
	for i := 0; i < 100; i++ {
		_, err = stmt.Exec(fmt.Sprintf("こんにちわ世界%03d", i), i%2)
		if err != nil {
			log.Fatal(err)
		}
	}
	tx.Commit()
}
