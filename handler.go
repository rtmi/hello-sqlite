package gocrm

import (
	"encoding/json"
	"fmt"
	"net/http"
	"strconv"
	"strings"
	"sync/atomic"
)

type DefaultHandler struct{}

func (h DefaultHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "Welcome to tutorial!")
}

type CompanyHandler struct{}

func (h CompanyHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	// TODO Use a CORS middleware
	w.Header().Add("Access-Control-Allow-Origin", "*") //w.Header.Get("Origin")
	w.Header().Add("Access-Control-Allow-Headers", "Content-Type")
	var ls Companies

	switch r.Method {
	case "POST":
		co, err := hydrate(r)
		if err != nil {
			http.Error(w, http.StatusText(http.StatusBadRequest), http.StatusBadRequest)
			return
		}
		AddCompany(co)

	case "GET":
		k, err := extractKey(r)
		if err != nil {
			http.Error(w, http.StatusText(http.StatusBadRequest), http.StatusBadRequest)
			return
		}
		if k != "" {
			ls = FetchCompanyByKey(k)
		}

	case "PUT":
		k, err := extractKey(r)
		if err != nil {
			http.Error(w, http.StatusText(http.StatusBadRequest), http.StatusBadRequest)
			return
		}
		if k != "" {
			co, err := hydrate(r)
			if err != nil {
				http.Error(w, http.StatusText(http.StatusBadRequest), http.StatusBadRequest)
				return
			}

			co.Id = k
			UpdateCompany(co)
		}

	}

	if len(ls.Data) == 0 {
		ls = FetchCompany()
	}
	json.NewEncoder(w).Encode(ls)
}

type ContactHandler struct{}

func (h ContactHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	ls := FetchContact()
	json.NewEncoder(w).Encode(ls)
}

type HeartbeatHandler struct{}

func (h HeartbeatHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	w.WriteHeader(http.StatusOK)
}

type ReadinessHandler struct {
	Ready *atomic.Value
}

func (h ReadinessHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	if h.Ready == nil || !h.Ready.Load().(bool) {
		http.Error(w, http.StatusText(http.StatusServiceUnavailable), http.StatusServiceUnavailable)
		return
	}
	w.WriteHeader(http.StatusOK)
}

func hydrate(r *http.Request) (Company, error) {
	at := Attr{}
	err := json.NewDecoder(r.Body).Decode(&at)
	if err != nil {
		return Company{}, err
	}
	return Company{Type: "companies", Attributes: at}, nil
}

func extractKey(r *http.Request) (string, error) {
	var (
		k   int
		err error
	)
	ar := strings.Split(r.URL.Path, "/")
	if len(ar) > 2 {
		if ar[2] == "" {
			// Key is NOT present
			return "", nil
		}

		// There may be a key in the URL
		k, err = strconv.Atoi(ar[2])
		if err != nil {
			return "", err
		}
	}
	i := strconv.Itoa(k)
	return i, nil
}
