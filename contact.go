package gocrm

type Contact struct {
	Firstname string `json:"Firstname"`
	Lastname  string `json:"Lastname"`
	Email     string `json:"Email"`
	Work      string `json:"Work"`
	Cell      string `json:"Cell"`
}

type Contacts []Contact
