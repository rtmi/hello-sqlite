# Hello-SQLite

CRM experiment to learn SQLite

![schema](https://patterns.github.io/tfx/images/er.svg)

## Quickstart

1. git clone https://github.com/patterns/hello-sqlite
2. cd hello-sqlite
3. SERVER=:8100 go run server/main.go
4. Use the httpie tool to check responses:

```
$ http --verbose POST localhost:8100/companies/ name=Animaniacs status:=77
POST /companies/ HTTP/1.1
Accept: application/json, */*
Accept-Encoding: gzip, deflate
Connection: keep-alive
Content-Length: 36
Content-Type: application/json
Host: localhost:8100
User-Agent: HTTPie/0.9.9

{
    "name": "Animaniacs",
    "status": 77
}

HTTP/1.1 200 OK
Access-Control-Allow-Headers: Content-Type
Access-Control-Allow-Origin: *
Content-Length: 163
Content-Type: text/plain; charset=utf-8
Date: Thu, 23 Aug 2018 00:57:45 GMT

{
    "data": [
        {
            "attributes": {
                "name": "Acme Inc",
                "status": 11
            },
            "id": "1",
            "type": "companies"
        },
        {
            "attributes": {
                "name": "Animaniacs",
                "status": 77
            },
            "id": "2",
            "type": "companies"
        }
    ]
}

```


## Credits

David Crawshaw's
 [SQLite presentation](https://crawshaw.io/blog/one-process-programming-notes)

Yasuhiro Matsumoto's
 [SQLite driver](https://github.com/mattn/go-sqlite3)
 [(LICENSE)](https://github.com/mattn/go-sqlite3/blob/master/LICENSE)

Kubernetes
 [tutorial](https://cloud.google.com/go/docs/tutorials/bookshelf-on-kubernetes-engine)

Ember's
 [JSONAPI convention](http://jsonapi.org/examples/)

REST
 [Httpie tool](https://httpie.org/)

