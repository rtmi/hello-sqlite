package main

import (
	"log"
	"net/http"
	"os"
	"sync/atomic"

	"github.com/patterns/gocrm"
)

func main() {
  readiness := gocrm.ReadinessHandler{}
  readiness.Ready = &atomic.Value{}
  readiness.Ready.Store(false)
  gocrm.Init()
  defer gocrm.Quit()
  readiness.Ready.Store(true)

  http.Handle("/", gocrm.DefaultHandler{})
  http.Handle("/companies/", gocrm.CompanyHandler{})
  http.Handle("/contacts/", gocrm.ContactHandler{})
  http.Handle("/health", gocrm.HeartbeatHandler{})
  http.Handle("/ready", readiness)

  server := os.Getenv("SERVER")
  if server == "" {
    server = ":8080"
  }
  log.Fatal(http.ListenAndServe(server, nil))
}

