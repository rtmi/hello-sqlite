package gocrm

type Attr struct {
	Name   string `json:"name"`
	Status int    `json:"status"`
}

type Company struct {
	Type       string `json:"type"`
	Id         string `json:"id"`
	Attributes Attr   `json:"attributes"`
}

type Companies struct {
	Data []Company `json:"data"`
}
